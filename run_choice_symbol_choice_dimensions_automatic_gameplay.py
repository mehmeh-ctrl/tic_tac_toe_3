# File run_choice_symbol_choice_dimensions_automatic_gameplay.py: Choose
# the dimensions of the matrix, choose the starting symbol and run an
# automatic gameplay.

from choice_starting_symbol_choice_dimensions_automatic_gameplay import TheGame

if __name__ == '__main__':
    game = TheGame()
