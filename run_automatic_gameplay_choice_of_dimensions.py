# File run_automatic_gameplay_choice_of_dimensions.py: The non-classic
# 2-player (simulated) tic-tac-toe console gameplay with the
# dimensions chosen by the player and the pseudorandom choice of the
# starting symbol (1 or 2 instead of "O" and "X") for the automatic
# building and testing purposes.

from choice_dimension_choice import TheGame

if __name__ == '__main__':
    game = TheGame()
