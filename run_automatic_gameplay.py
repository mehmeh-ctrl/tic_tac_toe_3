# File run_automatic_gameplay.py: The non-classic 2-player (simulated)
# tic-tac-toe console gameplay with the pseudorandomly chosen dimensions
# (from 3 to 101) and the pseudorandom choice of the starting symbol (1
# or 2 instead of "O" and "X") for the automatic building and testing
# purposes.

from choice_automatic import TheGame

if __name__ == '__main__':
    game = TheGame()
