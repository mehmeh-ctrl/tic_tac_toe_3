# File run.py:  The non-classic 2-player tic-tac-toe console game with the
# dimensions, which you can choose and the choice of the playing symbol
# (1 or 2 instead of "O" or "X").

from choice import TheGame

if __name__ == '__main__':
    game = TheGame()
