# File run_2_choices.py: Play against the computer (pseudorandomly),
# choose the grid's dimensions, the starting symbol and the symbol you
# want to play with.

from choice_2_choices import TheGame

if __name__ == '__main__':
    game = TheGame()
