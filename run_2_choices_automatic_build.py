# File run_2_choices_automatic_build.py: Pseudorandom choice of
# the starting symbol and the grid's dimensions (from 3 to 101).
# The version written for the automatic testing and building purposes.

from choice_2_choices_automatic_build import TheGame

if __name__ == '__main__':
    game = TheGame()
